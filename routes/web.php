<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    // Ruta para ver donde estan todos los trabajadores en la tabla
    Route::get('/empleados', 'EmpleadoController@index');

    Route::get('/empleados/create', 'EmpleadoController@create');

    Route::post('/empleados', 'EmpleadoController@store');

    Route::get('/empleados/{id}/edit', 'EmpleadoController@edit');

    Route::put('/empleados/{id}', 'EmpleadoController@update');

    Route::delete('/empleados/{id}', 'EmpleadoController@destroy');
















//    //mostrar el formulario de usuario nuevo
//    Route::get('/employees/create', 'EmployesControler@create');
//
//    //guardar usuario nuevo
//    Route::post('/employees', 'EmployeesController@store');
//
//    //mostrar el formulario de actualizar usuario
//
//    Route::get('/employees{id}/edit', 'EmployesControler@edit');
//
//    //guardar el usuario actualizado
//    Route::put('/employees/{id}', 'EmployesControler@update');
//
//    //eliminar un usuario en especifico
//    Route::delete('/employees/{id}', 'EmployesControler@destroy');


});

