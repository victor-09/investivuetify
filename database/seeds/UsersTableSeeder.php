<?php

use App\Models\Empleado;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empleado::create([
            "empleadoNombre" => "empleadoNombre",
            "apellidos" => "apellidos",
            "edad" => "edad",
            "cumpleanos" => "cumpleanos",
            "pais" => "pais",
            "empresa" => "empresa",
        ]);
        User::create([
            "username" => "admin",
            "password" => Hash::make('admin')
        ]);


    }
}
