<?php

namespace App\Http\Controllers;
use App\Models\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $empleados = Empleado::all();

        return Inertia::render('Empleados/Index', [
            'empleados' => $empleados
        ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Empleados/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $empleado = new Empleado();
        $empleado->empleadoNombre = $request->empleadoNombre;
        $empleado->apellidos = $request->apellidos;
        $empleado->edad = $request->edad;
        $empleado->cumpleanos = $request->cumpleanos;
        $empleado->pais = $request->pais;
        $empleado->empresa = $request->empresa;
        $empleado->save();
        DB::commit();


        return redirect('/empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $empleado = Empleado::where('id', $id)->first();

        return Inertia::render('Empleados/Edit', [
            'empleado' => $empleado
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $empleado = Empleado::where('id', $id)->first();
        $empleado->empleadoNombre = $request->empleadoNombre;
        $empleado->apellidos = $request->apellidos;
        $empleado->edad = $request->edad;
        $empleado->cumpleanos = $request->cumpleanos;
        $empleado->pais = $request->pais;
        $empleado->empresa = $request->empresa;
        $empleado->save();
        DB::commit();


        return redirect('/empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $empleado = Empleado::where('id', $id)->first();
        $empleado->delete();
        DB::commit();

        return redirect('/empleados');
    }
}
