<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Empleado extends Model
{
    use Notifiable;
    protected $fillable = ['empleadoNombre', 'apellidos', 'edad', 'cumpleanos', 'pais', 'empresa', ];
}
